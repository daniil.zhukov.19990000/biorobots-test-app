import { Module, VuexModule, Mutation, getModule } from 'vuex-module-decorators'
import store from '../../index'

@Module({
  dynamic: true, store, name: 'purse'
})
class Purse extends VuexModule {
  coins: number = 45

  @Mutation
  addCoins (quantity: number) {
    this.coins += quantity
  }

  @Mutation
  subtractCoins (quantity: number) {
    this.coins -= quantity
  }
}

export const PurseModule = getModule(Purse)
