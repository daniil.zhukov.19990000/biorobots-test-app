import { Module, VuexModule, Mutation, getModule } from 'vuex-module-decorators'
import store from '../../index'
import IDetail from './interfaces'
import Vue from 'vue'

@Module({ dynamic: true, store, name: 'production' })
class Production extends VuexModule {
  details: IDetail[] = [
    {
      alias: 'biomechanism',
      rusAlias: 'биомеханизм',
      cost: 7,
      quantityStock: 0,
      costOfSale: 5,
      quantityProduction: 0,
      need: 4,
      installed: []
    },
    {
      alias: 'processor',
      rusAlias: 'процессор',
      cost: 5,
      quantityStock: 0,
      costOfSale: 3,
      quantityProduction: 0,
      need: 4,
      installed: []
    },
    {
      alias: 'soul',
      rusAlias: 'душа',
      cost: 25,
      quantityStock: 0,
      costOfSale: 15,
      quantityProduction: 0,
      need: 1,
      installed: []
    }
  ]

  @Mutation
  install (payload: String) {
    for (const i in this.details) {
      if (this.details[i].alias === payload) {
        this.details[i].quantityStock += 1
        break
      }
    }
  }

  @Mutation
  sell (payload: String) {
    for (const i in this.details) {
      if (this.details[i].alias === payload) {
        this.details[i].quantityStock -= 1
        break
      }
    }
  }

  @Mutation
  onProduction (payload: any) {
    for (const i in this.details) {
      if (this.details[i].alias === payload.detail.alias) {
        this.details[i].quantityProduction += 1
        this.details[i].quantityStock -= 1
        Vue.set(this.details[i].installed, payload.index, true)
        break
      }
    }
  }

  @Mutation
  takeOffProduction (payload: any) {
    for (const i in this.details) {
      if (this.details[i].alias === payload.detail.alias) {
        this.details[i].quantityProduction -= 1
        this.details[i].quantityStock += 1
        Vue.set(this.details[i].installed, payload.index, false)
        break
      }
    }
  }

  @Mutation
  zeroOutInstalled () {
    for (const i in this.details) {
      this.details[i].installed = new Array(this.details[i].need).fill(false)
    }
  }

  @Mutation
  zeroOutQuantityProduction () {
    for (const i in this.details) {
      this.details[i].quantityProduction = 0
    }
  }
}

export const ProductionModule = getModule(Production)
