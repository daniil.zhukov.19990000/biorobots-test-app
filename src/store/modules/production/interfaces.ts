export default interface IDetail {
  alias: string,
  rusAlias: string,
  cost: number,
  quantityStock: number,
  costOfSale: number,
  quantityProduction: number,
  need: number,
  installed: boolean[]
}
